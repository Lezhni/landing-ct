$(document).ready(function() {

	$('input[name=phone]').mask('+375 (99) 999-99-99');

	$('.popup-btn').magnificPopup({
		type: 'inline'
	});

	$('form').submit(function() {

		form = $(this);
		var name = form.find('[name=name]').val();
		var phone = form.find('[name=phone]').val();
		$.post(
			'send.php',
			{name: name, phone: phone},
			function(data) {
				if (data == 'sended') {
					$.magnificPopup.open({
						items: {
							src: '#thanks',
							type: 'inline'
						},
						callbacks: {
							open: function() {
								setTimeout(function() {
									$.magnificPopup.close();
								}, 3000);
							}
						}
					});
				} else {
					alert('Ошибка отправки. Попробуйте снова');
				}
			}
		);
		return false;
	})
});